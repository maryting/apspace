export interface Qualification {
  StudentId: string;
  Name: string;
  Programme: string;
  yog: string;
  AwardAchieved: string;
}
