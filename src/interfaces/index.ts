export { Apcard } from './apcard';
export { Attendance } from './attendance';
export { AttendanceLegend } from './attendance-legend';
export { BusTrips, Trips } from './bus-trips';
export { ApuLocations, LocationsInterface } from './bus-locations';
export { ClassificationLegend } from './classfication-legend';
export { Course } from './courses';
export { CourseDetails } from './course-details';
export { DeterminationLegend } from './determination-legend';
import { DepName, UnavailruleDet } from './iconsult';
export { ExamSchedule } from './exam-schedule';
export { FeedbackData } from './feedback';
export { FeesBankDraft, FeesDetails, FeesSummary, FeesTotalSummary } from './fees';
export { Holidays, Holiday } from './holiday';
export {
  ConDetail, FreeSlotsLec, UnavailruleDet, Starttimes, Feedback,
  Upcomingcon, DetailpageStudent, FreeSlots, StaffName, DepName,
  LecGetRulesDet, GetRooms,
} from './iconsult';
export { IntakeListing } from './intake-listing';
export { InterimLegend } from './interim-legend';
export { LecturerTimetable } from './lecturer-timetable';
export { MPULegend } from './mpu-legend';
export { News } from './news';
export { Role, Settings } from './settings';
export { Sqa } from './sqa';
export { StaffDirectory } from './staff-directory';
export { StaffProfile } from './staff-profile';
export { StudentPhoto } from './student-photo';
export { StudentProfile } from './student-profile';
export { Subcourse } from './subcourses';
export { Timetable } from './timetable';
export { Qualification } from './qualification';
export { StudentConsentProfile } from './student-consent-profile';
export { AplcClassDescription } from './aplc-class-description';
export { AplcStudentBehaviour } from './aplc-student-behaviour';

