export interface FeedbackData {
  contactNo: string;
  message: string;
  platform: string;
  appVersion: string;
  screenSize: string;
}
