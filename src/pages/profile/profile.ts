import { Component, ElementRef } from '@angular/core';
import { App, IonicPage } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import { Role, StaffProfile, StudentPhoto, StudentProfile } from '../../interfaces';
import { AppAnimationProvider, SettingsProvider, WsApiProvider } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  photo$: Observable<StudentPhoto[]>;
  profile$: Observable<StudentProfile>;
  staffProfile$: Observable<StaffProfile[]>;
  visa$: Observable<any>;

  local: boolean = false;
  studentRole = false;

  constructor(
    private ws: WsApiProvider,
    private settings: SettingsProvider,
    private appAnimationProvider: AppAnimationProvider,
    private elRef: ElementRef,
    private app: App,
  ) { }

  ionViewDidLoad() {
    this.appAnimationProvider.addAnimationsToSections(this.elRef);
    const role = this.settings.get('role');
    if (role & Role.Student) {
      this.studentRole = true;
      this.photo$ = this.ws.get<StudentPhoto[]>('/student/photo', true);
      this.profile$ = this.ws.get<StudentProfile>('/student/profile', true);
      this.getProfile();
    } else if (role & (Role.Lecturer | Role.Admin)) {
      this.staffProfile$ = this.ws.get<StaffProfile[]>('/staff/profile', true);
    }
  }

  openStaffDirectoryInfo(id: string) {
    this.app.getRootNav().push('StaffDirectoryInfoPage', { id });
  }

  getProfile() {
    this.ws.get<StudentProfile>('/student/profile').pipe(
      tap(p => {
        if (p.COUNTRY === 'Malaysia') {
          this.local = true;
        } else {
          this.local = false;
          this.visa$ = this.getVisaStatus();
        }
      }),
    ).subscribe();
  }

  getVisaStatus() {
    return this.ws.get<any>('/student/visa_status');
  }

}
